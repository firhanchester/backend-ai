<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modback extends CI_Model {

    public function login($data){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username',$data['username']);
        $this->db->where('password',$data['password']);
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return['success' => TRUE , 'data' => $q->row_array()];
        }else{
            return['success' => FALSE , 'data' => ''];
        }
    }

    public function get_user_session($data){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username',$data['username']);      
        $this->db->where('email',$data['email']);      
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return['success' => TRUE , 'data' => $q->row_array()];
        }else{
            return['success' => FALSE , 'data' => ''];
        }
    }

}