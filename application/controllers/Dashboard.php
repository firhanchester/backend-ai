<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

class Dashboard extends CI_Controller {
	public function index()
	{
		$this->load->view('backend/header');
		$this->load->view('backend/sidebar');
		$this->load->view('backend/dashboard');
		$this->load->view('backend/footer');
	}
}
