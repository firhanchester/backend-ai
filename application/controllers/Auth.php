<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function index()
	{
		$this->load->view('backend/login');
	}

	public function action_login(){
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$data = array(
			'username' => $u,
			'password' => md5($p),
		);
		$exe = $this->modback->login($data);
		if($exe['success'] == TRUE){
			$this->session->set_userdata('login',$exe['data']);
			redirect(site_url('dashboard'));
		}else{
			redirect(site_url('auth'));
		}
	}

	public function logout(){

	}
}
